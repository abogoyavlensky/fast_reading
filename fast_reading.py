# -*- coding: utf-8 -*-
 
import Tkinter, tkFileDialog
import pygame
from pygame.locals import *
import sys
import os
 
 
class Words(object):
    def __init__(self, filename):
        if not os.path.exists(filename):
            sys.exit()
        with open(filename) as f:
            self.words = f.read().replace("\n", "  ").split(" ")
        self.position = 0
 
    def __iter__(self):
        return self
 
    def next(self):
        if len(self.words) > self.position:
            word = self.words[self.position]
            self.position += 1
            return word
        else:
            raise StopIteration
 
    def backward(self, ccount=5):
        if self.position - ccount > 0:
            self.position = self.position - ccount
        else:
            self.position = 0
 
    def forward(self, ccount=5):
        if len(self.words) > self.position + ccount:
            self.position = self.position + ccount
 
    def get_position(self):
        return self.position
 
class Reader(object):
    def __init__(self, sizex=1024, sizey=400, speed=4.0, filename=""):
        pygame.init()
        pygame.display.set_caption("Reader")
        self.screen = pygame.display.set_mode((sizex, sizey))
        self.sizex = sizex
        self.sizey = sizey
 
        self.background = pygame.Surface(self.screen.get_size())
        self.background = self.background.convert()
        self.background.fill((0, 0, 0))
 
        self.clock = pygame.time.Clock()
        self.speed = speed
        self.message = ""
        self.message_timer = 0
        self.paused = True
        self.words = Words(filename)
        self.word = ""
 
    def set_word(self, word):
        self.word = word
        return self.word
 
    def find_xcenter(self, label, width=None):
        if width is None:
            width = self.sizex
        return self.sizex / 2 - (label.get_width() / 2)
 
    def draw(self, word):
        self.screen.blit(self.background, (0, 0))
        if len(word.strip()) > 0:
            self.set_word(word)
        x, y = self.screen.get_size()
        word = self.word
        font = pygame.font.SysFont("comicsansms", 60)
        label = font.render(word.decode("utf-8"), 1, (255, 255, 255))
        xcenter = self.find_xcenter(label)
        self.screen.blit(label, (xcenter, 100))
 
    def tick(self):
        self.clock.tick(self.speed)
 
    def set_message(self, message, timeout=20):
        self.message = message
        self.message_timer = timeout
 
    def draw_message(self):
        if self.paused:
            message = "Paused, press space to continue. " + self.message
        else:
            message = self.message
        if not self.paused and self.message_timer < 0:
            return None
        font = pygame.font.SysFont("comicsansms", 30)
        label = font.render(message, 1, (100, 100, 100))
        self.screen.blit(label, (50, 200))
        self.message_timer -= 1
 
    def check_for_events(self, paused=False):
        def events(paused=paused):
            for event in pygame.event.get():
                if event.type == QUIT:
                    sys.exit()
                elif event.type == MOUSEBUTTONDOWN:
                    if paused:
                        return False
                    else:
                        return True
                elif event.type == KEYDOWN:
                    if event.key == K_SPACE:
                        if paused:
                           return False
                        else:
                            return True
                    elif event.key == K_UP:
                        self.speed += 0.1
                        self.set_message("Speed is %d ch/m" % (self.speed * 60))
                    elif event.key == K_DOWN:
                        self.speed -= 0.1
                        self.set_message("Speed is %d ch/m" % (self.speed * 60))
                    elif event.key == K_LEFT:
                        self.words.backward()
                        self.set_message("Moved to word %d" % (self.words.get_position()))
                    elif event.key == K_RIGHT:
                        self.words.forward()
                        self.set_message("Moved to word %d" % (self.words.get_position()))
                    elif event.key == K_ESCAPE:
                        sys.exit()
            if paused:
                return True
 
        while events(self.paused):
            self.paused = True
            self.draw(self.word)
            self.tick()
            self.draw_message()
            pygame.display.update()
        self.paused = False
 
    def mainloop(self):
        for word in self.words:
            self.check_for_events()
            self.draw(word)
            self.tick()
            self.draw_message()
            pygame.display.update()
 
 
if __name__ == "__main__":
    tk = Tkinter.Tk()
    filename = tkFileDialog.askopenfilename()
    tk.withdraw()
 
    Reader(filename=filename).mainloop()
